package com.lightappsoft.tagtalk;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lightappsoft.tagtalk.dto.FindCriteriaDto;
import com.lightappsoft.tagtalk.dto.MessageType;
import com.lightappsoft.tagtalk.model.GenderCriteria;
import com.lightappsoft.tagtalk.model.Message;
import com.lightappsoft.tagtalk.model.impl.TagImpl;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

@SpringBootTest
public class JsonTest {

    @Autowired
    private ObjectMapper objectMapper;

    @SneakyThrows
    @Test
    public void findCriteriaDto(){
        FindCriteriaDto dto = new FindCriteriaDto();
        dto.setTags(Arrays.asList(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pussy")));
        dto.setCoincidenceCount(2);
        dto.setMyGender(GenderCriteria.MAN);
        dto.setInterlocutorGender(GenderCriteria.ANY);
        dto.setMessageType(MessageType.FIND_CRITERIA_DTO);
        String text = objectMapper.writeValueAsString(dto);
        System.out.println(text.replace(",",",\n"));
        Message restore = objectMapper.readValue(text, Message.class);
        switch (restore.getMessageType()){
            case None: case FIND_CRITERIA_DTO:
                FindCriteriaDto findCriteriaDto = (FindCriteriaDto) restore;
                System.out.println(restore);
                Assertions.assertTrue(true);
                break;
            default:
                Assertions.fail();
        }
    }
}
