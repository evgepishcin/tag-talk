package com.lightappsoft.tagtalk.service;

import com.lightappsoft.tagtalk.model.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;


@SpringBootTest
public class SuitablePersonFinderTest {

    @Autowired
    private SuitablePersonFinder suitablePersonFinder;

    private static class TagImpl implements Tag {
        private String val;

        public TagImpl(String val) {
            this.val = val;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TagImpl tag = (TagImpl) o;
            return val.equals(tag.val);
        }

        @Override
        public int hashCode() {
            return Objects.hash(val);
        }

        @Override
        public String toString() {
            return val;
        }
    }

    private static class InterlocutorImpl extends Interlocutor {

        private String name;

        public InterlocutorImpl() {
        }

        public InterlocutorImpl(String name) {
            this.name = name;
        }

        @Override
        public void sendMessage(Message message) {
            //dummy
        }

        @Override
        public boolean isConnected() {
            return true; //dummy
        }

        @Override
        public String toString() {
            if (name != null){
                return name;
            }
            return super.toString();
        }
    }

    @Test
    public void findCouples(){
        List<Interlocutor> interlocutors = new ArrayList<>();
        List<Interlocutor> rightInter = new ArrayList<>();
        Interlocutor interlocutor = new InterlocutorImpl("me");
        interlocutor.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria = new FindCriteria();
        findCriteria.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria.setCoincidenceCount(2);
        findCriteria.setMyGender(GenderCriteria.MAN);
        findCriteria.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor.setCriteria(findCriteria);
        interlocutors.add(interlocutor);

        Interlocutor interlocutor1 = new InterlocutorImpl("interlocutor1");
        interlocutor1.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria1 = new FindCriteria();
        findCriteria1.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria1.setCoincidenceCount(2);
        findCriteria1.setMyGender(GenderCriteria.WOMAN);
        findCriteria1.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor1.setCriteria(findCriteria1);
        interlocutors.add(interlocutor1);
        rightInter.add(interlocutor1);

        Interlocutor interlocutor2 = new InterlocutorImpl("interlocutor2");
        interlocutor2.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria2 = new FindCriteria();
        findCriteria2.setTags(Set.of(new TagImpl("life"),
                new TagImpl("job"),
                new TagImpl("money")));
        findCriteria2.setCoincidenceCount(2);
        findCriteria2.setMyGender(GenderCriteria.WOMAN);
        findCriteria2.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor2.setCriteria(findCriteria2);
        interlocutors.add(interlocutor2);

        Interlocutor interlocutor3 = new InterlocutorImpl("interlocutor3");
        interlocutor3.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria3 = new FindCriteria();
        findCriteria3.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria3.setCoincidenceCount(2);
        findCriteria3.setMyGender(GenderCriteria.MAN);
        findCriteria3.setInterlocutorGender(GenderCriteria.WOMAN);
        interlocutor3.setCriteria(findCriteria3);
        interlocutors.add(interlocutor3);

        Interlocutor interlocutor4 = new InterlocutorImpl("interlocutor4");
        interlocutor4.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria4 = new FindCriteria();
        findCriteria4.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria4.setCoincidenceCount(4);
        findCriteria4.setMyGender(GenderCriteria.WOMAN);
        findCriteria4.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor4.setCriteria(findCriteria4);
        interlocutors.add(interlocutor4);
        rightInter.add(interlocutor4);

        Interlocutor interlocutor5 = new InterlocutorImpl("interlocutor5");
        interlocutor5.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria5 = new FindCriteria();
        findCriteria5.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("eat"),
                new TagImpl("milk"),
                new TagImpl("chocolate")));
        findCriteria5.setCoincidenceCount(1);
        findCriteria5.setMyGender(GenderCriteria.WOMAN);
        findCriteria5.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor5.setCriteria(findCriteria5);
        interlocutors.add(interlocutor5);

        Interlocutor interlocutor6 = new InterlocutorImpl("interlocutor6");
        interlocutor6.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria6 = new FindCriteria();
        findCriteria6.setTags(Set.of(new TagImpl("cream"),
                new TagImpl("dog"),
                new TagImpl("dolphin"),
                new TagImpl("pussy")));
        findCriteria6.setCoincidenceCount(1);
        findCriteria6.setMyGender(GenderCriteria.WOMAN);
        findCriteria6.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor6.setCriteria(findCriteria6);
        interlocutors.add(interlocutor6);
        rightInter.add(interlocutor6);

        Interlocutor interlocutor7 = new InterlocutorImpl("interlocutor7");
        interlocutor7.setStatus(InterlocutorStatus.NEW);
        FindCriteria findCriteria7 = new FindCriteria();
        findCriteria7.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria7.setCoincidenceCount(2);
        findCriteria7.setMyGender(GenderCriteria.WOMAN);
        findCriteria7.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor7.setCriteria(findCriteria7);
        interlocutors.add(interlocutor7);

        Interlocutor interlocutor8 = new InterlocutorImpl("interlocutor8");
        interlocutor8.setStatus(InterlocutorStatus.DIALOG);
        FindCriteria findCriteria8 = new FindCriteria();
        findCriteria8.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria8.setCoincidenceCount(2);
        findCriteria8.setMyGender(GenderCriteria.WOMAN);
        findCriteria8.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor8.setCriteria(findCriteria8);
        interlocutors.add(interlocutor8);

        List<Interlocutor> find = suitablePersonFinder.findCouples(interlocutor, interlocutors);

        System.out.println("\ninterlocutors:");
        for (Interlocutor in: interlocutors){
            System.out.println(in);
        }
        System.out.println("\nexpect:");
        for (Interlocutor in: rightInter){
            System.out.println(in);
        }
        System.out.println("\nactual:");
        for (Interlocutor in: find){
            System.out.println(in);
        }
        Assertions.assertTrue(rightInter.size() == find.size() &&
                rightInter.containsAll(find) && find.containsAll(rightInter));
    }

    @Test
    public void findCouples_genderManFindMan(){
        List<Interlocutor> interlocutors = new ArrayList<>();
        List<Interlocutor> rightInter = new ArrayList<>();
        Interlocutor interlocutor = new InterlocutorImpl("me");
        interlocutor.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria = new FindCriteria();
        findCriteria.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria.setCoincidenceCount(2);
        findCriteria.setMyGender(GenderCriteria.MAN);
        findCriteria.setInterlocutorGender(GenderCriteria.MAN);
        interlocutor.setCriteria(findCriteria);
        interlocutors.add(interlocutor);

        Interlocutor interlocutor1 = new InterlocutorImpl("interlocutor1");
        interlocutor1.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria1 = new FindCriteria();
        findCriteria1.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria1.setCoincidenceCount(2);
        findCriteria1.setMyGender(GenderCriteria.MAN);
        findCriteria1.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor1.setCriteria(findCriteria1);
        interlocutors.add(interlocutor1);
        rightInter.add(interlocutor1);

        Interlocutor interlocutor2 = new InterlocutorImpl("interlocutor2");
        interlocutor2.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria2 = new FindCriteria();
        findCriteria2.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria2.setCoincidenceCount(2);
        findCriteria2.setMyGender(GenderCriteria.WOMAN);
        findCriteria2.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor2.setCriteria(findCriteria2);
        interlocutors.add(interlocutor2);

        Interlocutor interlocutor3 = new InterlocutorImpl("interlocutor3");
        interlocutor3.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria3 = new FindCriteria();
        findCriteria3.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria3.setCoincidenceCount(2);
        findCriteria3.setMyGender(GenderCriteria.WOMAN);
        findCriteria3.setInterlocutorGender(GenderCriteria.MAN);
        interlocutor3.setCriteria(findCriteria3);
        interlocutors.add(interlocutor3);

        Interlocutor interlocutor4 = new InterlocutorImpl("interlocutor4");
        interlocutor4.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria4 = new FindCriteria();
        findCriteria4.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria4.setCoincidenceCount(2);
        findCriteria4.setMyGender(GenderCriteria.MAN);
        findCriteria4.setInterlocutorGender(GenderCriteria.WOMAN);
        interlocutor4.setCriteria(findCriteria4);
        interlocutors.add(interlocutor4);

        Interlocutor interlocutor5 = new InterlocutorImpl("interlocutor5");
        interlocutor5.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria5 = new FindCriteria();
        findCriteria5.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria5.setCoincidenceCount(2);
        findCriteria5.setMyGender(GenderCriteria.MAN);
        findCriteria5.setInterlocutorGender(GenderCriteria.MAN);
        interlocutor5.setCriteria(findCriteria5);
        interlocutors.add(interlocutor5);
        rightInter.add(interlocutor5);

        Interlocutor interlocutor6 = new InterlocutorImpl("interlocutor6");
        interlocutor6.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria6 = new FindCriteria();
        findCriteria6.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria6.setCoincidenceCount(2);
        findCriteria6.setMyGender(GenderCriteria.MAN);
        findCriteria6.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor6.setCriteria(findCriteria6);
        interlocutors.add(interlocutor6);
        rightInter.add(interlocutor6);

        Interlocutor interlocutor7 = new InterlocutorImpl("interlocutor7");
        interlocutor7.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria7 = new FindCriteria();
        findCriteria7.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria7.setCoincidenceCount(2);
        findCriteria7.setMyGender(GenderCriteria.ANY);
        findCriteria7.setInterlocutorGender(GenderCriteria.MAN);
        interlocutor7.setCriteria(findCriteria7);
        interlocutors.add(interlocutor7);

        Interlocutor interlocutor8 = new InterlocutorImpl("interlocutor8");
        interlocutor8.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria8 = new FindCriteria();
        findCriteria8.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria8.setCoincidenceCount(2);
        findCriteria8.setMyGender(GenderCriteria.ANY);
        findCriteria8.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor8.setCriteria(findCriteria8);
        interlocutors.add(interlocutor8);

        Interlocutor interlocutor9 = new InterlocutorImpl("interlocutor9");
        interlocutor9.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria9 = new FindCriteria();
        findCriteria9.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria9.setCoincidenceCount(2);
        findCriteria9.setMyGender(GenderCriteria.ANY);
        findCriteria9.setInterlocutorGender(GenderCriteria.WOMAN);
        interlocutor9.setCriteria(findCriteria9);
        interlocutors.add(interlocutor9);

        List<Interlocutor> find = suitablePersonFinder.findCouples(interlocutor, interlocutors);

        System.out.println("\ninterlocutors:");
        for (Interlocutor in: interlocutors){
            System.out.println(in);
        }
        System.out.println("\nexpect:");
        for (Interlocutor in: rightInter){
            System.out.println(in);
        }
        System.out.println("\nactual:");
        for (Interlocutor in: find){
            System.out.println(in);
        }
        Assertions.assertTrue(rightInter.size() == find.size() &&
                rightInter.containsAll(find) && find.containsAll(rightInter));
    }

    @Test
    public void findCouples_genderAnyFindWoman(){
        List<Interlocutor> interlocutors = new ArrayList<>();
        List<Interlocutor> rightInter = new ArrayList<>();
        Interlocutor interlocutor = new InterlocutorImpl("me");
        interlocutor.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria = new FindCriteria();
        findCriteria.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria.setCoincidenceCount(2);
        findCriteria.setMyGender(GenderCriteria.ANY);
        findCriteria.setInterlocutorGender(GenderCriteria.WOMAN);
        interlocutor.setCriteria(findCriteria);
        interlocutors.add(interlocutor);

        Interlocutor interlocutor1 = new InterlocutorImpl("interlocutor1");
        interlocutor1.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria1 = new FindCriteria();
        findCriteria1.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria1.setCoincidenceCount(2);
        findCriteria1.setMyGender(GenderCriteria.MAN);
        findCriteria1.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor1.setCriteria(findCriteria1);
        interlocutors.add(interlocutor1);

        Interlocutor interlocutor2 = new InterlocutorImpl("interlocutor2");
        interlocutor2.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria2 = new FindCriteria();
        findCriteria2.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria2.setCoincidenceCount(2);
        findCriteria2.setMyGender(GenderCriteria.WOMAN);
        findCriteria2.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor2.setCriteria(findCriteria2);
        interlocutors.add(interlocutor2);
        rightInter.add(interlocutor2);

        Interlocutor interlocutor3 = new InterlocutorImpl("interlocutor3");
        interlocutor3.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria3 = new FindCriteria();
        findCriteria3.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria3.setCoincidenceCount(2);
        findCriteria3.setMyGender(GenderCriteria.WOMAN);
        findCriteria3.setInterlocutorGender(GenderCriteria.MAN);
        interlocutor3.setCriteria(findCriteria3);
        interlocutors.add(interlocutor3);

        Interlocutor interlocutor4 = new InterlocutorImpl("interlocutor4");
        interlocutor4.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria4 = new FindCriteria();
        findCriteria4.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria4.setCoincidenceCount(2);
        findCriteria4.setMyGender(GenderCriteria.MAN);
        findCriteria4.setInterlocutorGender(GenderCriteria.WOMAN);
        interlocutor4.setCriteria(findCriteria4);
        interlocutors.add(interlocutor4);

        Interlocutor interlocutor5 = new InterlocutorImpl("interlocutor5");
        interlocutor5.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria5 = new FindCriteria();
        findCriteria5.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria5.setCoincidenceCount(2);
        findCriteria5.setMyGender(GenderCriteria.MAN);
        findCriteria5.setInterlocutorGender(GenderCriteria.MAN);
        interlocutor5.setCriteria(findCriteria5);
        interlocutors.add(interlocutor5);

        Interlocutor interlocutor6 = new InterlocutorImpl("interlocutor6");
        interlocutor6.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria6 = new FindCriteria();
        findCriteria6.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria6.setCoincidenceCount(2);
        findCriteria6.setMyGender(GenderCriteria.MAN);
        findCriteria6.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor6.setCriteria(findCriteria6);
        interlocutors.add(interlocutor6);

        Interlocutor interlocutor7 = new InterlocutorImpl("interlocutor7");
        interlocutor7.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria7 = new FindCriteria();
        findCriteria7.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria7.setCoincidenceCount(2);
        findCriteria7.setMyGender(GenderCriteria.ANY);
        findCriteria7.setInterlocutorGender(GenderCriteria.MAN);
        interlocutor7.setCriteria(findCriteria7);
        interlocutors.add(interlocutor7);

        Interlocutor interlocutor8 = new InterlocutorImpl("interlocutor8");
        interlocutor8.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria8 = new FindCriteria();
        findCriteria8.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria8.setCoincidenceCount(2);
        findCriteria8.setMyGender(GenderCriteria.ANY);
        findCriteria8.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor8.setCriteria(findCriteria8);
        interlocutors.add(interlocutor8);

        Interlocutor interlocutor9 = new InterlocutorImpl("interlocutor9");
        interlocutor9.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria9 = new FindCriteria();
        findCriteria9.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria9.setCoincidenceCount(2);
        findCriteria9.setMyGender(GenderCriteria.ANY);
        findCriteria9.setInterlocutorGender(GenderCriteria.WOMAN);
        interlocutor9.setCriteria(findCriteria9);
        interlocutors.add(interlocutor9);

        Interlocutor interlocutor10 = new InterlocutorImpl("interlocutor10");
        interlocutor10.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria10 = new FindCriteria();
        findCriteria10.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria10.setCoincidenceCount(2);
        findCriteria10.setMyGender(GenderCriteria.WOMAN);
        findCriteria10.setInterlocutorGender(GenderCriteria.WOMAN);
        interlocutor10.setCriteria(findCriteria10);
        interlocutors.add(interlocutor10);

        List<Interlocutor> find = suitablePersonFinder.findCouples(interlocutor, interlocutors);

        System.out.println("\ninterlocutors:");
        for (Interlocutor in: interlocutors){
            System.out.println(in);
        }
        System.out.println("\nexpect:");
        for (Interlocutor in: rightInter){
            System.out.println(in);
        }
        System.out.println("\nactual:");
        for (Interlocutor in: find){
            System.out.println(in);
        }
        Assertions.assertTrue(rightInter.size() == find.size() &&
                rightInter.containsAll(find) && find.containsAll(rightInter));
    }

    @Test
    public void findCouples_tagCount_1(){
        List<Interlocutor> interlocutors = new ArrayList<>();
        List<Interlocutor> rightInter = new ArrayList<>();
        Interlocutor interlocutor = new InterlocutorImpl("me");
        interlocutor.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria = new FindCriteria();
        findCriteria.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria.setCoincidenceCount(2);
        findCriteria.setMyGender(GenderCriteria.MAN);
        findCriteria.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor.setCriteria(findCriteria);
        interlocutors.add(interlocutor);

        Interlocutor interlocutor1 = new InterlocutorImpl("interlocutor1");
        interlocutor1.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria1 = new FindCriteria();
        findCriteria1.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria1.setCoincidenceCount(2);
        findCriteria1.setMyGender(GenderCriteria.MAN);
        findCriteria1.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor1.setCriteria(findCriteria1);
        interlocutors.add(interlocutor1);
        rightInter.add(interlocutor1);

        Interlocutor interlocutor2 = new InterlocutorImpl("interlocutor2");
        interlocutor2.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria2 = new FindCriteria();
        findCriteria2.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria2.setCoincidenceCount(4);
        findCriteria2.setMyGender(GenderCriteria.MAN);
        findCriteria2.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor2.setCriteria(findCriteria2);
        interlocutors.add(interlocutor2);
        rightInter.add(interlocutor2);

        Interlocutor interlocutor3 = new InterlocutorImpl("interlocutor3");
        interlocutor3.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria3 = new FindCriteria();
        findCriteria3.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria3.setCoincidenceCount(1);
        findCriteria3.setMyGender(GenderCriteria.MAN);
        findCriteria3.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor3.setCriteria(findCriteria3);
        interlocutors.add(interlocutor3);
        rightInter.add(interlocutor3);

        Interlocutor interlocutor4 = new InterlocutorImpl("interlocutor4");
        interlocutor4.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria4 = new FindCriteria();
        findCriteria4.setTags(Set.of(new TagImpl("food"),
                new TagImpl("dog"),
                new TagImpl("pizza"),
                new TagImpl("burger")));
        findCriteria4.setCoincidenceCount(1);
        findCriteria4.setMyGender(GenderCriteria.MAN);
        findCriteria4.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor4.setCriteria(findCriteria4);
        interlocutors.add(interlocutor4);

        Interlocutor interlocutor5 = new InterlocutorImpl("interlocutor5");
        interlocutor5.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria5 = new FindCriteria();
        findCriteria5.setTags(Set.of(new TagImpl("mm"),
                new TagImpl("dog"),
                new TagImpl("zz"),
                new TagImpl("pussy")));
        findCriteria5.setCoincidenceCount(1);
        findCriteria5.setMyGender(GenderCriteria.MAN);
        findCriteria5.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor5.setCriteria(findCriteria5);
        interlocutors.add(interlocutor5);
        rightInter.add(interlocutor5);

        List<Interlocutor> find = suitablePersonFinder.findCouples(interlocutor, interlocutors);

        System.out.println("\ninterlocutors:");
        for (Interlocutor in: interlocutors){
            System.out.println(in);
        }
        System.out.println("\nexpect:");
        for (Interlocutor in: rightInter){
            System.out.println(in);
        }
        System.out.println("\nactual:");
        for (Interlocutor in: find){
            System.out.println(in);
        }
        Assertions.assertTrue(rightInter.size() == find.size() &&
                rightInter.containsAll(find) && find.containsAll(rightInter));
    }

    @Test
    public void findCouples_commonTagOrder(){
        List<Interlocutor> interlocutors = new ArrayList<>();
        List<Interlocutor> rightInter = new ArrayList<>();
        Interlocutor interlocutor = new InterlocutorImpl("me");
        interlocutor.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria = new FindCriteria();
        findCriteria.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria.setCoincidenceCount(1);
        findCriteria.setMyGender(GenderCriteria.MAN);
        findCriteria.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor.setCriteria(findCriteria);
        interlocutors.add(interlocutor);

        Interlocutor interlocutor1 = new InterlocutorImpl("interlocutor1");
        interlocutor1.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria1 = new FindCriteria();
        findCriteria1.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog")));
        findCriteria1.setCoincidenceCount(1);
        findCriteria1.setMyGender(GenderCriteria.MAN);
        findCriteria1.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor1.setCriteria(findCriteria1);
        interlocutors.add(interlocutor1);

        Interlocutor interlocutor2 = new InterlocutorImpl("interlocutor2");
        interlocutor2.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria2 = new FindCriteria();
        findCriteria2.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pets"),
                new TagImpl("pussy")));
        findCriteria2.setCoincidenceCount(1);
        findCriteria2.setMyGender(GenderCriteria.MAN);
        findCriteria2.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor2.setCriteria(findCriteria2);
        interlocutors.add(interlocutor2);

        Interlocutor interlocutor3 = new InterlocutorImpl("interlocutor3");
        interlocutor3.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria3 = new FindCriteria();
        findCriteria3.setTags(Set.of(new TagImpl("cat"),
                new TagImpl("dog"),
                new TagImpl("pussy")));
        findCriteria3.setCoincidenceCount(1);
        findCriteria3.setMyGender(GenderCriteria.MAN);
        findCriteria3.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor3.setCriteria(findCriteria3);
        interlocutors.add(interlocutor3);

        Interlocutor interlocutor4 = new InterlocutorImpl("interlocutor4");
        interlocutor4.setStatus(InterlocutorStatus.FIND);
        FindCriteria findCriteria4 = new FindCriteria();
        findCriteria4.setTags(Set.of(new TagImpl("cat")));
        findCriteria4.setCoincidenceCount(1);
        findCriteria4.setMyGender(GenderCriteria.MAN);
        findCriteria4.setInterlocutorGender(GenderCriteria.ANY);
        interlocutor4.setCriteria(findCriteria4);
        interlocutors.add(interlocutor4);

        rightInter.add(interlocutor4);
        rightInter.add(interlocutor1);
        rightInter.add(interlocutor3);
        rightInter.add(interlocutor2);

        List<Interlocutor> find = suitablePersonFinder.findCouples(interlocutor, interlocutors);

        System.out.println("SEQUENCE IMPORTANT IN THIS CASE!");
        System.out.println("\ninterlocutors:");
        for (Interlocutor in: interlocutors){
            System.out.println(in);
        }
        System.out.println("\nexpect:");
        for (Interlocutor in: rightInter){
            System.out.println(in);
        }
        System.out.println("\nactual:");
        for (Interlocutor in: find){
            System.out.println(in);
        }
        Assertions.assertEquals(rightInter, find);
    }
}