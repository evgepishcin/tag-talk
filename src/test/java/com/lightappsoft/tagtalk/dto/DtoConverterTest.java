package com.lightappsoft.tagtalk.dto;

import com.lightappsoft.tagtalk.model.FindCriteria;
import com.lightappsoft.tagtalk.model.GenderCriteria;
import com.lightappsoft.tagtalk.model.impl.TagImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class DtoConverterTest {

    @Autowired
    DtoConverter dtoConverter;

    @Test
    public void convertFromDto(){
        FindCriteriaDto findCriteriaDto = new FindCriteriaDto();
        findCriteriaDto.setMessageType(MessageType.FIND_CRITERIA_DTO);
        findCriteriaDto.setTags(Arrays.asList(new TagImpl("test1"),
                new TagImpl("test2")));
        findCriteriaDto.setCoincidenceCount(3);
        findCriteriaDto.setMyGender(GenderCriteria.MAN);
        findCriteriaDto.setInterlocutorGender(GenderCriteria.ANY);

        FindCriteria findCriteria = dtoConverter.convertFromDto(findCriteriaDto);
        assertTrue(findCriteriaDto.getTags().size() == findCriteria.getTags().size() &&
                findCriteriaDto.getTags().containsAll(findCriteria.getTags()) &&
                findCriteria.getTags().containsAll(findCriteriaDto.getTags()));
        assertEquals(findCriteriaDto.getCoincidenceCount(), findCriteria.getCoincidenceCount());
        assertEquals(findCriteriaDto.getMyGender(), findCriteria.getMyGender());
        assertEquals(findCriteriaDto.getInterlocutorGender(), findCriteria.getInterlocutorGender());
    }

}