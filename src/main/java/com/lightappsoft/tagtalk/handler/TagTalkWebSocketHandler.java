package com.lightappsoft.tagtalk.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lightappsoft.tagtalk.model.Message;
import com.lightappsoft.tagtalk.service.ClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

@Slf4j
@Component
public class TagTalkWebSocketHandler extends TextWebSocketHandler {

    @Autowired
    private ClientService clientService;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        log.trace("new client connected to server from "+session.getRemoteAddress());
        clientService.addNewClient(session);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        String text = message.getPayload();
        log.trace("message from client "+session.getRemoteAddress()+": "+text);
        Message userMessage = objectMapper.readValue(text, Message.class);
        clientService.newMessage(session, userMessage);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        log.trace("client disconnect from "+session.getRemoteAddress());
        clientService.deleteClient(session);
    }
}
