package com.lightappsoft.tagtalk.model;

public enum MessageAuthor {
    ME,
    STRANGER
}
