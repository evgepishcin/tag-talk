package com.lightappsoft.tagtalk.model;

public enum InterlocutorStatus {

    /**
     * The user has just connected and has not yet started
     * looking for an interlocutor and is not in dialogue mode
     */
    NEW,

    /**
     * The user is looking for an interlocutor
     */
    FIND,

    /**
     * The user is in dialogue mode
     */
    DIALOG
}
