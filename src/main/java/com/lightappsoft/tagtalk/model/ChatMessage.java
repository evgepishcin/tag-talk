package com.lightappsoft.tagtalk.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatMessage extends Message{

    public static final String chatMessage = "chatMessage";

    private String text;
    private MessageAuthor author;
    private String uuid;
}
