package com.lightappsoft.tagtalk.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Dialog {
    private Interlocutor interlocutor1;
    private Interlocutor interlocutor2;

    public boolean contain(Interlocutor interlocutor){
        return interlocutor.equals(interlocutor1) || interlocutor.equals(interlocutor2);
    }
}
