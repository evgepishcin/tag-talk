package com.lightappsoft.tagtalk.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class FindCriteria {
    private Set<Tag> tags;
    private Integer coincidenceCount;
    private GenderCriteria myGender;
    private GenderCriteria interlocutorGender;

    @Override
    public String toString() {
        return "tags:"+tags.toString()+
                ",coincidenceCount: "+coincidenceCount+
                ",myGender: "+myGender.toString()+
                ",interlocutorGender: "+interlocutorGender.toString();
    }
}
