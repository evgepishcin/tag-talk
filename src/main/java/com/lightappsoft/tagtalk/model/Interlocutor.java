package com.lightappsoft.tagtalk.model;


public abstract class Interlocutor {

    protected FindCriteria criteria;
    protected InterlocutorStatus status = InterlocutorStatus.NEW;

    public abstract void sendMessage(Message message) throws ClientConnectionException;

    public abstract boolean isConnected();

    public FindCriteria getCriteria() {
        return criteria;
    }

    public void setCriteria(FindCriteria criteria) {
        this.criteria = criteria;
    }

    public InterlocutorStatus getStatus() {
        return status;
    }

    public void setStatus(InterlocutorStatus status) {
        this.status = status;
    }
}
