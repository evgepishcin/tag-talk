package com.lightappsoft.tagtalk.model.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lightappsoft.tagtalk.model.ClientConnectionException;
import com.lightappsoft.tagtalk.model.Interlocutor;
import com.lightappsoft.tagtalk.model.Message;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

@Component
@Scope("prototype")
public class InterlocutorImpl extends Interlocutor {

    private WebSocketSession socketSession;

    @Autowired
    private ObjectMapper objectMapper;

    @SneakyThrows
    @Override
    public void sendMessage(Message message) throws ClientConnectionException {
        String text = objectMapper.writeValueAsString(message);
        WebSocketMessage<String> textMessage = new TextMessage(text);
        try {
            socketSession.sendMessage(textMessage);
        } catch (Exception e){
            throw new ClientConnectionException("Can't send message to interlocutor", e);
        }
    }

    @Override
    public boolean isConnected() {
        return socketSession.isOpen();
    }

    public WebSocketSession getSocketSession() {
        return socketSession;
    }

    public void setSocketSession(WebSocketSession socketSession) {
        this.socketSession = socketSession;
    }

    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }
}
