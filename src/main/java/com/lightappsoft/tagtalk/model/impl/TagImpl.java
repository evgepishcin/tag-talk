package com.lightappsoft.tagtalk.model.impl;

import com.lightappsoft.tagtalk.model.Tag;

import java.util.Objects;

public class TagImpl implements Tag {

    private String name = "";

    public TagImpl() {
    }

    public TagImpl(String name) {
        this.name = name.toUpperCase();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.toUpperCase();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TagImpl tag = (TagImpl) o;
        return name.equals(tag.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return name;
    }
}
