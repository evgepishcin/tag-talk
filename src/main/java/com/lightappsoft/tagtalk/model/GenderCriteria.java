package com.lightappsoft.tagtalk.model;

public enum GenderCriteria {
    MAN,
    WOMAN,
    ANY
}
