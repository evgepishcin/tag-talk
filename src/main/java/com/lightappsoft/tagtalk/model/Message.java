package com.lightappsoft.tagtalk.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.lightappsoft.tagtalk.dto.ActionDto;
import com.lightappsoft.tagtalk.dto.DialogInfoDto;
import com.lightappsoft.tagtalk.dto.FindCriteriaDto;
import com.lightappsoft.tagtalk.dto.MessageType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "messageType", visible = true)
@JsonSubTypes(value = {
        @JsonSubTypes.Type(value = FindCriteriaDto.class, name = FindCriteriaDto.findCriteriaDto),
        @JsonSubTypes.Type(value = DialogInfoDto.class, name = DialogInfoDto.dialogInfoDto),
        @JsonSubTypes.Type(value = ActionDto.class, name = ActionDto.actionDto),
        @JsonSubTypes.Type(value = ChatMessage.class, name = ChatMessage.chatMessage)
})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private MessageType messageType;
}
