package com.lightappsoft.tagtalk.configuration;

import com.lightappsoft.tagtalk.handler.TagTalkWebSocketHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
public class WebSocketConfiguration implements WebSocketConfigurer {

    private static final String END_POINT = "/";

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry webSocketHandlerRegistry) {
        webSocketHandlerRegistry.addHandler(getTagTalkWebSocketHandler(), END_POINT)
                .setAllowedOrigins("*");
    }

    @Bean
    public WebSocketHandler getTagTalkWebSocketHandler() {
        return new TagTalkWebSocketHandler();
    }
}
