package com.lightappsoft.tagtalk.dto;

import com.lightappsoft.tagtalk.model.Message;

public interface DtoConverter {

    <T> T convertFromDto(Message message);
}
