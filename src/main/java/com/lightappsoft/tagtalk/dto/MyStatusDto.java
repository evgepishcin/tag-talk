package com.lightappsoft.tagtalk.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lightappsoft.tagtalk.model.InterlocutorStatus;
import com.lightappsoft.tagtalk.model.Message;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MyStatusDto extends Message {

    @JsonIgnore
    public static final String myStatusDto = "myStatusDto";

    private InterlocutorStatus status;

    public MyStatusDto(InterlocutorStatus status){
        this.status = status;
    }
}
