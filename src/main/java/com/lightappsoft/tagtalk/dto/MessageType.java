package com.lightappsoft.tagtalk.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.lightappsoft.tagtalk.model.ChatMessage;

import java.util.stream.Stream;

public enum MessageType {
    None(""),
    FIND_CRITERIA_DTO(FindCriteriaDto.findCriteriaDto),
    DIALOG_INFO_DTO(DialogInfoDto.dialogInfoDto),
    ACTION_DTO(ActionDto.actionDto),
    CHAT_MESSAGE(ChatMessage.chatMessage);

    private String value;

    MessageType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    @JsonValue
    final String value(){
        return value;
    }

    @JsonCreator
    public static MessageType forValue(String val){
        return Stream.of(values()).filter(value -> value.toString().equals(val)).findFirst().orElse(None);
    }
}
