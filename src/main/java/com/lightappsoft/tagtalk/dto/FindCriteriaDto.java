package com.lightappsoft.tagtalk.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lightappsoft.tagtalk.model.GenderCriteria;
import com.lightappsoft.tagtalk.model.Message;
import com.lightappsoft.tagtalk.model.impl.TagImpl;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class FindCriteriaDto extends Message {

    @JsonIgnore
    public static final String findCriteriaDto = "findCriteriaDto";

    private List<TagImpl> tags;
    private Integer coincidenceCount;
    private GenderCriteria myGender;
    private GenderCriteria interlocutorGender;
}
