package com.lightappsoft.tagtalk.dto.impl;

import com.lightappsoft.tagtalk.dto.DtoConverter;
import com.lightappsoft.tagtalk.dto.FindCriteriaDto;
import com.lightappsoft.tagtalk.model.FindCriteria;
import com.lightappsoft.tagtalk.model.Message;
import org.springframework.stereotype.Component;

import java.util.HashSet;

@Component
public class DtoConverterImpl implements DtoConverter {


    @SuppressWarnings(value = "unchecked")
    @Override
    public <T> T convertFromDto(Message message){
        switch (message.getMessageType()){
            case FIND_CRITERIA_DTO:
                return (T) fromFindCriteriaDto((FindCriteriaDto) message);
            default:
                return (T) message;
        }
    }

    private FindCriteria fromFindCriteriaDto(FindCriteriaDto findCriteriaDto){
        FindCriteria findCriteria = new FindCriteria();
        findCriteria.setTags(new HashSet<>(findCriteriaDto.getTags()));
        findCriteria.setCoincidenceCount(findCriteriaDto.getCoincidenceCount());
        findCriteria.setMyGender(findCriteriaDto.getMyGender());
        findCriteria.setInterlocutorGender(findCriteriaDto.getInterlocutorGender());
        return findCriteria;
    }
}
