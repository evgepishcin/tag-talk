package com.lightappsoft.tagtalk.dto;

public interface Action {
    String STOP_DIALOG = "STOP_DIALOG";
    String STOP_SEARCH = "STOP_SEARCH";
}
