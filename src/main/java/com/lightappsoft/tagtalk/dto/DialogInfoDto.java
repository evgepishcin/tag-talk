package com.lightappsoft.tagtalk.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lightappsoft.tagtalk.model.GenderCriteria;
import com.lightappsoft.tagtalk.model.Interlocutor;
import com.lightappsoft.tagtalk.model.Message;
import com.lightappsoft.tagtalk.model.impl.TagImpl;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class DialogInfoDto extends Message {

    @JsonIgnore
    public static final String dialogInfoDto = "dialogInfoDto";

    private List<TagImpl> myTags;
    private List<TagImpl> interlocutorTags;
    private GenderCriteria myGender;
    private GenderCriteria interlocutorGender;

    public DialogInfoDto(Interlocutor me, Interlocutor stranger) {
        myTags = me.getCriteria().getTags().stream()
                .map(x -> new TagImpl(((TagImpl) x).getName())).collect(Collectors.toList());
        interlocutorTags = stranger.getCriteria().getTags().stream()
                .map(x -> new TagImpl(((TagImpl)x).getName())).collect(Collectors.toList());
        myGender = me.getCriteria().getMyGender();
        interlocutorGender = stranger.getCriteria().getMyGender();
    }
}
