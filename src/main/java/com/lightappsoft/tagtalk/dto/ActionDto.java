package com.lightappsoft.tagtalk.dto;

import com.lightappsoft.tagtalk.model.Message;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ActionDto extends Message {

    public static final String actionDto = "actionDto";

    private String action;
}
