package com.lightappsoft.tagtalk.service.impl;

import com.lightappsoft.tagtalk.dto.DialogInfoDto;
import com.lightappsoft.tagtalk.dto.MyStatusDto;
import com.lightappsoft.tagtalk.model.*;
import com.lightappsoft.tagtalk.repos.LockVariables;
import com.lightappsoft.tagtalk.service.DialogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class DialogServiceImpl implements DialogService {

    private List<Dialog> dialogs = new ArrayList<>();


    @Override
    public void createDialog(Interlocutor interlocutor1, Interlocutor interlocutor2){
        if (interlocutor1.isConnected() && interlocutor2.isConnected()){
            log.debug("create dialog with: "+interlocutor1+", and: "+interlocutor2);
            synchronized (LockVariables.lockInterlocutors){
                if (interlocutor1.getStatus().equals(InterlocutorStatus.FIND) &&
                interlocutor2.getStatus().equals(InterlocutorStatus.FIND)) {
                    interlocutor1.setStatus(InterlocutorStatus.DIALOG);
                    interlocutor2.setStatus(InterlocutorStatus.DIALOG);
                    dialogs.add(new Dialog(interlocutor1, interlocutor2));
                }
            }
            DialogInfoDto forInt1 = new DialogInfoDto(interlocutor1, interlocutor2);
            DialogInfoDto forInt2 = new DialogInfoDto(interlocutor2, interlocutor1);
            try {
                interlocutor1.sendMessage(forInt1);
                interlocutor2.sendMessage(forInt2);
            } catch (ClientConnectionException e){
                log.warn("can't send DialogInfoMessage", e);
                destroyDialog(interlocutor1);
            }
        }
        log.info("dialogs: "+dialogs.size());
    }

    @Override
    public void destroyDialog(Interlocutor interlocutor) {
        log.debug("destroy dialog with: "+interlocutor);
        Dialog dialog = getDialogByInterlocutor(interlocutor);
        synchronized (LockVariables.lockInterlocutors){
            if (dialog != null){
                dialog.getInterlocutor1().setStatus(InterlocutorStatus.NEW);
                dialog.getInterlocutor2().setStatus(InterlocutorStatus.NEW);
                dialogs.remove(dialog);
            }
        }
        if (dialog != null) {
            if (dialog.getInterlocutor1().isConnected()){
                dialog.getInterlocutor1().sendMessage(new MyStatusDto(dialog.getInterlocutor1().getStatus()));
            }
            if (dialog.getInterlocutor2().isConnected()){
                dialog.getInterlocutor2().sendMessage(new MyStatusDto(dialog.getInterlocutor2().getStatus()));

            }
        }
        log.info("dialogs: "+dialogs.size());
    }

    @Override
    public void sendMessage(Interlocutor interlocutor, ChatMessage chatMessage) {
        log.debug("send dialog message from: "+interlocutor);
        Dialog dialog = getDialogByInterlocutor(interlocutor);
        if (dialog != null) {
            if (!dialog.getInterlocutor1().isConnected() || !dialog.getInterlocutor2().isConnected()){
                destroyDialog(interlocutor);
                return;
            }
            Interlocutor stranger;
            if (dialog.getInterlocutor2() == interlocutor){
                stranger = dialog.getInterlocutor1();
            }else {
                stranger = dialog.getInterlocutor2();
            }
            try{
                chatMessage.setAuthor(MessageAuthor.STRANGER);
                stranger.sendMessage(chatMessage);
                chatMessage.setAuthor(MessageAuthor.ME);
                interlocutor.sendMessage(chatMessage);
            } catch (ClientConnectionException e){
                log.warn("can't send message to interlocutor in dialog", e);
                destroyDialog(interlocutor);
            }
        }
    }

    private Dialog getDialogByInterlocutor(Interlocutor interlocutor){
        return dialogs.stream()
                .filter(x -> x.contain(interlocutor))
                .findFirst().orElse(null);
    }
}
