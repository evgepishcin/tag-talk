package com.lightappsoft.tagtalk.service;

import com.lightappsoft.tagtalk.dto.Action;
import com.lightappsoft.tagtalk.dto.ActionDto;
import com.lightappsoft.tagtalk.dto.DtoConverter;
import com.lightappsoft.tagtalk.model.*;
import com.lightappsoft.tagtalk.repos.LockVariables;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MessageService {

    @Autowired
    private DtoConverter dtoConverter;

    @Autowired
    private DialogFinder dialogFinder;

    @Autowired
    private DialogService dialogService;

    public void handleMessage(Interlocutor interlocutor, Message message){
        switch (message.getMessageType()){
            case FIND_CRITERIA_DTO:
                FindCriteria findCriteria = dtoConverter.convertFromDto(message);
                findDialog(interlocutor, findCriteria);
                break;
            case ACTION_DTO:
                action(interlocutor, dtoConverter.convertFromDto(message));
                break;
            case CHAT_MESSAGE:
                ChatMessage chatMessage = dtoConverter.convertFromDto(message);
                dialogService.sendMessage(interlocutor, chatMessage);
        }
    }

    private void findDialog(Interlocutor interlocutor, FindCriteria findCriteria){
        log.trace("client find dialog: "+interlocutor+": "+findCriteria);
        synchronized (LockVariables.lockInterlocutors){
            if (interlocutor.getStatus().equals(InterlocutorStatus.NEW)){
                interlocutor.setStatus(InterlocutorStatus.FIND);
                interlocutor.setCriteria(findCriteria);
            }
        }
        dialogFinder.find();
    }

    private void action(Interlocutor interlocutor, ActionDto actionDto) {
        if (actionDto.getAction().equals(Action.STOP_DIALOG)){
            dialogService.destroyDialog(interlocutor);
        }else if (actionDto.getAction().equals(Action.STOP_SEARCH)){
            if (interlocutor.getStatus().equals(InterlocutorStatus.FIND)){
                synchronized (LockVariables.lockInterlocutors){
                    if (interlocutor.getStatus().equals(InterlocutorStatus.FIND)){
                        interlocutor.setStatus(InterlocutorStatus.NEW);
                        log.trace("stop search for dialog, interlocutor: "+interlocutor);
                    }
                }
            }
        }
    }
}
