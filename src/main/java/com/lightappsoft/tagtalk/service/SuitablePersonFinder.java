package com.lightappsoft.tagtalk.service;

import com.lightappsoft.tagtalk.model.GenderCriteria;
import com.lightappsoft.tagtalk.model.Interlocutor;
import com.lightappsoft.tagtalk.model.InterlocutorStatus;
import com.lightappsoft.tagtalk.model.Tag;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class SuitablePersonFinder {

    public List<Interlocutor> findCouples(Interlocutor interlocutor,
                                          Collection<? extends Interlocutor> collection) {
        List<Interlocutor> canSuitable;
        Stream<? extends Interlocutor> suitableStream = collection.stream();
        suitableStream = suitableStream
                .filter(x -> x != interlocutor)
                .filter(x -> x.getStatus().equals(InterlocutorStatus.FIND))
                .filter(Interlocutor::isConnected);
        GenderCriteria myGender = interlocutor.getCriteria().getMyGender();
        GenderCriteria partnerGender = interlocutor.getCriteria().getInterlocutorGender();
        if (partnerGender.equals(GenderCriteria.WOMAN)) {
            suitableStream = suitableStream
                    .filter(x -> x.getCriteria().getMyGender().equals(GenderCriteria.WOMAN));
        } else if (partnerGender.equals(GenderCriteria.MAN)) {
            suitableStream = suitableStream
                    .filter(x -> x.getCriteria().getMyGender().equals(GenderCriteria.MAN));
        }
        suitableStream = suitableStream
                .filter(x -> x.getCriteria().getInterlocutorGender().equals(myGender) ||
                        x.getCriteria().getInterlocutorGender().equals(GenderCriteria.ANY));
        suitableStream = suitableStream
                .filter(x -> commonTagCount(x.getCriteria().getTags(),
                        interlocutor.getCriteria().getTags()) >= Math.max(
                        x.getCriteria().getCoincidenceCount(), interlocutor.getCriteria().getCoincidenceCount()
                ));
        List<CommonTagInterlocutor> commonTagsInter = suitableStream
                .map(x -> new CommonTagInterlocutor(x, (commonTagCount(x.getCriteria().getTags(),
                        interlocutor.getCriteria().getTags()))))
                .sorted(new CommonTagInterlocutorComparator()).collect(Collectors.toList());
        return commonTagsInter.stream()
                .map(x -> x.interlocutor)
                .collect(Collectors.toList());
    }

    //TODO ОБЯЗАТЕЛЬНО ПРОТЕСТИРОВАТЬ!!
    public Interlocutor randomMaxCoincidenceTag(Interlocutor interlocutor,
                                                      Collection<? extends Interlocutor> interlocutors) {
        List<CommonTagInterlocutor> commonTagsInter = interlocutors.stream()
                .map(x -> new CommonTagInterlocutor(x, (commonTagCount(x.getCriteria().getTags(),
                        interlocutor.getCriteria().getTags()))))
                .sorted(new CommonTagInterlocutorComparator()).collect(Collectors.toList());
        long maxCoincidenceCount = commonTagsInter.get(commonTagsInter.size()-1).tagCount;
        List<Interlocutor> find = commonTagsInter.stream()
                .filter(x -> x.tagCount == maxCoincidenceCount)
                .map(x -> x.interlocutor)
                .collect(Collectors.toList());
        int size = find.size();
        Random random = new Random();
        int val = random.nextInt(size);
        return find.get(val);
    }

    private long commonTagCount(Collection<Tag> col1, Collection<Tag> col2) {
        return col1.stream()
                .filter(col2::contains)
                .count();
    }


    private static class CommonTagInterlocutor {

        private final Interlocutor interlocutor;
        private final long tagCount;

        public CommonTagInterlocutor(Interlocutor interlocutor, long tagCount) {
            this.interlocutor = interlocutor;
            this.tagCount = tagCount;
        }
    }

    private static class CommonTagInterlocutorComparator implements Comparator<CommonTagInterlocutor> {
        @Override
        public int compare(CommonTagInterlocutor o1, CommonTagInterlocutor o2) {
            return Long.compare(o1.tagCount, o2.tagCount);
        }
    }

}
