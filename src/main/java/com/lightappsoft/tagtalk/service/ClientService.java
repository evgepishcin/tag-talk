package com.lightappsoft.tagtalk.service;

import com.lightappsoft.tagtalk.model.Interlocutor;
import com.lightappsoft.tagtalk.model.InterlocutorStatus;
import com.lightappsoft.tagtalk.model.Message;
import com.lightappsoft.tagtalk.model.impl.InterlocutorImpl;
import com.lightappsoft.tagtalk.repos.InterlocutorRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

@Slf4j
@Service
public class ClientService implements InterlocutorRepository {

    private volatile Map<WebSocketSession, Interlocutor> clientMap;
    private volatile List<Interlocutor> interlocutors;

    @Autowired
    private BeanFactory beanFactory;

    @Autowired
    private DialogService dialogService;

    @Autowired
    private MessageService messageService;

    @PostConstruct
    public void init(){
        clientMap = new ConcurrentHashMap<>();
        interlocutors = new CopyOnWriteArrayList<>();
    }

    public synchronized void addNewClient(WebSocketSession session){
        InterlocutorImpl interlocutor = beanFactory.getBean(InterlocutorImpl.class);
        interlocutor.setStatus(InterlocutorStatus.NEW);
        interlocutor.setSocketSession(session);
        clientMap.put(session, interlocutor);
        interlocutors.add(interlocutor);
        log.trace("add new client:"+interlocutor+", address:"+session.getRemoteAddress());
        log.info("online: "+interlocutors.size());
    }

    public synchronized void deleteClient(WebSocketSession session) {
        Interlocutor interlocutor = clientMap.get(session);
        if (interlocutor.getStatus().equals(InterlocutorStatus.DIALOG)){
            dialogService.destroyDialog(interlocutor);
        }
        clientMap.remove(session);
        interlocutors.removeIf(x -> ((InterlocutorImpl)x).getSocketSession().equals(session));
        log.trace("delete client:"+interlocutor+", address:"+session.getRemoteAddress());
        log.info("online: "+interlocutors.size());
    }

    public void newMessage(WebSocketSession session, Message message) {
        Interlocutor interlocutor = clientMap.get(session);
        log.debug("new message from client:"+interlocutor+", address:"+session.getRemoteAddress());
        messageService.handleMessage(interlocutor, message);
    }

    @Override
    public List<Interlocutor> getInterlocutors() {
        return interlocutors;
    }

}
