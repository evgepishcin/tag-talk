package com.lightappsoft.tagtalk.service;

import com.lightappsoft.tagtalk.model.Interlocutor;
import com.lightappsoft.tagtalk.model.InterlocutorStatus;
import com.lightappsoft.tagtalk.repos.InterlocutorRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class DialogFinder {

    @Autowired
    private InterlocutorRepository interlocutorRepository;

    @Autowired
    private SuitablePersonFinder suitablePersonFinder;

    @Autowired
    private DialogService dialogService;

    private volatile boolean inSearch = false;

    private static final Object lock = new Object();

    public void find(){
        synchronized (lock){
            if (!inSearch){
                inSearch = true;
                Thread thread = new Thread(new Finder());
                thread.start();
            }else {
                log.debug("dialog finder already run");
            }
        }
    }

    private class Finder implements Runnable {
        @Override
        public void run() {
            log.debug("dialog finder run");
            startSearch();
            synchronized (lock){
                inSearch = false;
            }
            log.debug("dialog finder stop");
        }

        private void startSearch(){
            List<Interlocutor> interlocutors = new ArrayList<>(interlocutorRepository.getInterlocutors());
            interlocutors.removeIf(x -> !x.getStatus().equals(InterlocutorStatus.FIND));
            while (interlocutors.size() > 0 &&
            !Thread.currentThread().isInterrupted()){
                try {
                    search(interlocutors);
                } catch (Exception e){
                    log.error("can't search interlocutors", e);
                }
                interlocutors = new ArrayList<>(interlocutorRepository.getInterlocutors());
                interlocutors.removeIf(x -> !x.getStatus().equals(InterlocutorStatus.FIND));
            }
        }

        private void search(List<Interlocutor> interlocutors){
            for (Interlocutor interlocutor: interlocutors){
                List<Interlocutor> findPartners =
                        suitablePersonFinder.findCouples(interlocutor, interlocutors);
                if (findPartners.size()>0){
                    Interlocutor find = suitablePersonFinder.randomMaxCoincidenceTag(interlocutor, findPartners);
                    if (!interlocutor.isConnected() || !find.isConnected()){
                        continue;
                    }
                    dialogService.createDialog(interlocutor, find);
                }
            }
        }
    }
}
