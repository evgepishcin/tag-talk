package com.lightappsoft.tagtalk.service;

import com.lightappsoft.tagtalk.model.ChatMessage;
import com.lightappsoft.tagtalk.model.Interlocutor;

public interface DialogService {

    void createDialog(Interlocutor interlocutor1, Interlocutor interlocutor2);
    void destroyDialog(Interlocutor interlocutor);
    void sendMessage(Interlocutor interlocutor, ChatMessage chatMessage);
}
