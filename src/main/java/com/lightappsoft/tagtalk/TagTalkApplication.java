package com.lightappsoft.tagtalk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TagTalkApplication {

	public static void main(String[] args) {
		SpringApplication.run(TagTalkApplication.class, args);
	}

}
