package com.lightappsoft.tagtalk.repos;

import com.lightappsoft.tagtalk.model.Interlocutor;

import java.util.List;

public interface InterlocutorRepository {

    List<Interlocutor> getInterlocutors();
}
